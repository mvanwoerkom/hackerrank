-module(solution).
-export([main/0]).

main() ->
  {ok, [M, N]} = io:fread("", "~d~d"),
  Magazine = read_line(M),
  Note = read_line(N),
  check_magazine(Magazine, Note),
  true.


check_magazine(Magazine, Note) ->
  Map = word_map(Magazine),
  Result = check_note(Note, Map),
  io:format("~s~n", [Result]),
  ok.


word_map(Magazine) ->
  word_map(Magazine, maps:new()).

word_map([], Map) ->
  Map;
word_map([H|T], Map) ->
  case maps:is_key(H, Map) of
    true ->
      Value = maps:get(H, Map),
      Map2 = maps:update(H, Value+1, Map);
    false ->
      Map2 = maps:put(H, 1, Map)
  end,
  word_map(T, Map2).


check_note([], _Map) ->
  "Yes";
check_note([H|T], Map) ->
  case maps:is_key(H, Map) of
    false ->
      "No";
    true ->
      Value = maps:get(H, Map),
      case Value < 1 of
        true ->
          "No";
        false ->
          Map2 = maps:update(H, Value-1, Map),
          check_note(T, Map2)
      end
  end.


read_line(N) ->
  Fmt = unicode:characters_to_list(lists:duplicate(N, "~s")),
  {ok, L} = io:fread("", Fmt),
  L.

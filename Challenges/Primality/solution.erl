-module(solution).
-export([main/0]).


main() ->
  {ok, [P]} = io:fread("", "~d"),
  L = read_list(P),
  lists:foreach(fun(N) ->
    Result = primality(N),
    io:format("~s~n", [Result])
  end, L),
  true.


primality(1) ->
  "Not prime";
primality(2) ->
  "Prime";
primality(N) when N rem 2 == 0 ->
  "Not prime";
primality(N) ->
  primality(3, N).

primality(D, N) when D*D > N ->  % D >= 3, N >= 3, N is odd
  "Prime";
primality(D, N) when N rem D == 0 ->
  "Not prime";
primality(D, N) ->
  primality(D+2, N).


read_list(N) ->
  Fmt = unicode:characters_to_list(lists:duplicate(N, "~d")),
  {ok, L} = io:fread("", Fmt),
  L.

# Frequency Queries

## Problem
https://www.hackerrank.com/challenges/frequency-queries/problem

## Solution

### Data structure
The `(3, z)` operation needs us to keep track of how  many integers are 
there with frequency z. 

So we keep track of how often we recorded an integer x in the 
map `A` : x -> freq(x). 

We also have the reverse map `AR` : Freq -> freq{ X | freq(X) = Freq }

### Time outs
Seemed straight-forward at first sight, but at one point I got time outs. :-(

So I reduced the solution to just read the input and write some fixed values.

This means the operations on the data structure should not contribute to the 
computation time, it should be I/O mainly.

Still got time outs. Dang!

Input seemed optimized enough, so it seemed to be the N single
`io:format` statements for output.

So this is one of the few problems so far, where I had to replace N
calls to `io:format` with a single one for a pre-calculated list `Out`, 
hoping that some sort of N times set up / tear down per single `io:format` call
was the culprit. 

And it seems it was. Gotcha.

Note: I used the property that `io:format` can handle sublists, here
of form `"<Result>\n"`.

### Finishing the task

So I re-enabled the code for `(1, x)` and `(2, y)` operations and 
started to weed out my final bugs.

It turned out that in `dec_ar/2` I decremented the count in `AR` for `FreqM`
instead of incrementing it.

Glad I cut the earlier quite large `eval` function into `inc_a/2`, `inc_ar/2`,
`dec_a/2` and `dec_ar/2`.

## After the task
F1rst Erl4ng p0st!

## Keywords
hash maps

-module(solution).
-export([main/0]).

main() ->
  {ok, [Q]} = io:fread("", "~d"),
  L = read_data(Q),
  eval(L),
  true.


eval(L) ->
  % X -> freq(X)
  A = maps:new(),
  % #X -> #{X|freq(X)=#X}
  AR = maps:new(),
  Out = [],
  eval(L, A, AR, Out).

eval([], _A, _AR, Out) ->
  io:format("~s", [lists:reverse(Out)]);
eval([1, X|T], A, AR, Out) ->
  {Freq, A2} = inc_a(X, A),
  AR2 = inc_ar(Freq, AR),
  eval(T, A2, AR2, Out);
eval([2, Y|T], A, AR, Out) ->
  {Freq, A2} = dec_a(Y, A),
  AR2 = dec_ar(Freq, AR),
  eval(T, A2, AR2, Out);
eval([3, Z|T], A, AR, Out) ->
  case maps:is_key(Z, AR) of
    true ->
      Out2 = ["1\n"|Out];
    false ->
      Out2 = ["0\n"|Out]
  end,
  eval(T, A, AR, Out2).


inc_a(X, A) ->
  case maps:is_key(X, A) of
    false ->
      Freq = 1;
    true ->
      Freq = maps:get(X, A) + 1
  end,
  A2 = maps:put(X, Freq, A),
  {Freq, A2}.


inc_ar(Freq, AR) ->
  % inc Freq
  AR2 = maps:update_with(Freq, fun(V) ->
    V+1
  end, 1, AR),
  % dec Freq-1
  FreqM = Freq-1,
  case maps:is_key(FreqM, AR2) of
    false ->
      AR2;
    true ->
      Val = maps:get(FreqM, AR2),
      case Val > 1 of
        false ->
          maps:remove(FreqM, AR2);
        true ->
          maps:update(FreqM, Val-1, AR2)
      end
  end.


dec_a(Y, A) ->
  case maps:is_key(Y, A) of
    false ->
      {none, A};
    true ->
      Freq = maps:get(Y, A),
      case Freq > 1 of
        false ->
          A2 = maps:remove(Y, A);
        true ->
          A2 = maps:update(Y, Freq-1, A)
      end,
      {Freq, A2}
    end.


dec_ar(none, AR) ->
  AR;
dec_ar(Freq, AR) ->
  % dec Freq
  Val = maps:get(Freq, AR),
  case Val > 1 of
    false ->
      AR2 = maps:remove(Freq, AR);
    true ->
      AR2 = maps:update(Freq, Val-1, AR)
  end,
  % inc Freq-1
  FreqM = Freq-1,
  case FreqM > 0 of
    false ->
      AR2;
    true ->
      maps:update_with(FreqM, fun(V) ->
        V+1
      end, 1, AR2)
  end.


read_data(N) ->
  Fmt = unicode:characters_to_list(lists:duplicate(N, "~d~d")),
  {ok, L} = io:fread("", Fmt),
  L.

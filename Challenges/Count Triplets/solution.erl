-module(solution).
-export([main/0]).


main() ->
  {ok, [N, R]} = io:fread("", "~d~d"),
  L = read_list(N),
  Result = count(R, L),
  io:format("~p~n", [Result]),
  true.

% see https://www.hackerrank.com/challenges/count-triplets-1/forum/comments/461579
count(R, L) ->
  Found = 0,
  Map = maps:new(),
  MapPairs = maps:new(),
  count(lists:reverse(L), R, Found, Map, MapPairs).

count([], _R, Found, _Map, _MapPairs) ->
  Found;
count([H|T], R, Found, Map, MapPairs) ->
  RH = R*H,
  case maps:is_key(RH, MapPairs) of
    true ->
      % found (H, (RH, R*RH))
      Found2 = Found + maps:get(RH, MapPairs);
    false ->
      Found2 = Found
  end,
  case maps:is_key(RH, Map) of
    true ->
      % found (H, RH)
      Val = maps:get(H, MapPairs, 0) + maps:get(RH, Map),
      MapPairs2 = maps:put(H, Val, MapPairs);
    false ->
      MapPairs2 = MapPairs
  end,
  % update single value
  Val2 = maps:get(H, Map, 0) + 1,
  Map2 = maps:put(H, Val2, Map),
  count(T, R, Found2, Map2, MapPairs2).


read_list(N) ->
  Fmt = unicode:characters_to_list(lists:duplicate(N, "~d")),
  {ok, L} = io:fread("", Fmt),
  L.

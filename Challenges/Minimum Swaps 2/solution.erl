-module(solution).
-export([main/0]).

main() ->
  {ok, [N]} = io:fread("", "~d"),
  L = read_list(N),
  A = array(L),
  % the list of integers might have gaps, so we need to determine the sorted list
  S = lists:sort(L),
  B = inv_array(S),
  Result = cycle_sort(N, B, A),
  io:format("~p~n", [Result]),
  true.


% detect cycles in permutation 
% decompose cycle of length k into k-1 transpositions (swaps)

cycle_sort(N, B, A) ->
  I = 1,
  Swaps = 0,
  cycle_sort(I, N, Swaps, B, A).

cycle_sort(I, N, Swaps, _B, _A) when I > N ->
  Swaps;
cycle_sort(I, N, Swaps, B, A) ->
  AI = maps:get(I, A),
  BI = maps:get(AI, B),
  % goal: A[BI] = AI
  case BI =:= I of
    true ->
      % position I is correct, check next position
      cycle_sort(I+1, N, Swaps, B, A);
    false ->
      {K, A2} = cycle(I, AI, BI, B, A),
      cycle_sort(I+1, N, Swaps+(K-1), B, A2)
  end.


% follow cycle starting at I, correcting A[BI] = AI
cycle(I, AI, BI, B, A) ->
  cycle(I, AI, BI, 0, B, A).

cycle(I, AI, BI, K, _B, A) when BI =:= I ->
  A2 = maps:update(BI, AI, A),
  {K+1, A2};
cycle(I, AI, BI, K, B, A) ->
  AJ = maps:get(BI, A),
  A2 = maps:update(BI, AI, A),
  BJ = maps:get(AJ, B),
  cycle(I, AJ, BJ, K+1, B, A2). 


% list to map : I -> E_I
array(L) ->
  array(L, 1, maps:new()).

array([], _I, Map) ->
  Map;
array([H|T], I, Map) ->
  Map2 = maps:put(I, H, Map),
  array(T, I+1, Map2).


% list to map : E_I -> I
inv_array(L) ->
  inv_array(L, 1, maps:new()).

inv_array([], _I, Map) ->
  Map;
inv_array([H|T], I, Map) ->
  Map2 = maps:put(H, I, Map),
  inv_array(T, I+1, Map2).


read_list(N) ->
  Fmt = unicode:characters_to_list(lists:duplicate(N, "~d")),
  {ok, L} = io:fread("", Fmt),
  L.

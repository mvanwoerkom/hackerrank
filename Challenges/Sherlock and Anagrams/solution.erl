-module(solution).
-export([main/0]).

main() ->
  {ok, [Q]} = io:fread("", "~d"),
  Queries = read_line(Q),
  lists:foreach(fun(S) ->
    Result = sherlockAndAnagrams(S),
    io:format("~p~n", [Result])
  end, Queries),
  true.


sherlockAndAnagrams(S) ->
  N = length(S),
  lists:foldl(fun(Length, Sum) ->
    Sum + check_substrings(N, S, Length)
  end, 0, lists:seq(1, N-1)).


% check substrings of length "Length"
check_substrings(N, S, Length) ->
  Positions = lists:seq(0, N-Length),
  Substrings = [string:slice(S, Start, Length) || Start <- Positions],

  % for comparison we sort each substring
  ComparablesList = [lists:sort(S) || S <- Substrings],

  % perhaps unnecessary optimization for faster access
  ComparablesMap = to_map(ComparablesList),

  Indices = lists:seq(1, N-Length+1),
  Pairs = [{I1, I2} || I1 <- Indices, I2 <- Indices, I1 < I2],
  lists:foldl(fun(Pair, Sum) ->
    {I1, I2} = Pair,
    C1 = maps:get(I1, ComparablesMap),
    C2 = maps:get(I2, ComparablesMap),
    Sum + check_pair(C1, C2)
  end, 0, Pairs).


% convert list to indexed map
to_map(L) ->
  Start = 1,
  Map = maps:new(),
  to_map(L, Start, Map).

to_map([], _Position, Map) ->
  Map;
to_map([H|T], Position, Map) ->
  Map2 = maps:put(Position, H, Map),
  to_map(T, Position+1, Map2).


check_pair(C1, C2) ->
  case C1 == C2 of
    true -> 
      1;
    false ->
      0
  end.


read_line(N) ->
  Fmt = unicode:characters_to_list(lists:duplicate(N, "~s")),
  {ok, L} = io:fread("", Fmt),
  L.

-module(solution).
-export([main/0]).


main() ->
  {ok, [Q]} = io:fread("", "~d"),
  L = read_list(Q),
  lists:foreach(fun(N) ->
    Result = flippingBits(N),
    io:format("~p~n", [Result])
  end, L),
  true.


flippingBits(N) ->
  Mask_32 = 16#ffffffff,
  N bxor Mask_32.


read_list(N) ->
  Fmt = unicode:characters_to_list(lists:duplicate(N, "~d")),
  {ok, L} = io:fread("", Fmt),
  L.

-module(solution).
-export([main/0]).


main() ->
  {ok, [Q]} = io:fread("", "~d"),
  L0 = read_list(2*Q),
  L = pairs(L0),
  P2C0 = maps:new(),  % map person to circle id
  C0 = maps:new(),    % map circle id to {list of persons, size}
  ID0 = 1,            % next available id
  Max0 = 0,           % largest size seen so far
  State0 = {P2C0, C0, ID0, Max0},
  lists:foldl(fun({QA, QB}, State) ->
    State2 = update(QA, QB, State),
    {_, _, _, Max2} = State2,
    io:format("~p~n", [Max2]),
    State2
  end, State0, L),
  true.


update(QA, QB, State) ->
  {P2C, _, _, _} = State,
  IDCA = maps:get(QA, P2C, undefined),
  IDCB = maps:get(QB, P2C, undefined),
  update(QA, QB, IDCA, IDCB, State).

update(QA, QB, undefined, undefined, {P2C, C, ID, Max}) ->
  % new circle CAB
  CAB = [QA, QB],
  Size = 2,
  C2 = maps:put(ID, {CAB, Size}, C),
  P2C2 = maps:put(QA, ID, P2C),
  P2C3 = maps:put(QB, ID, P2C2),
  ID2 = ID + 1,
  Max2 = max(Max, Size),
  {P2C3, C2, ID2, Max2};
update(QA, QB, IDCA, undefined, {P2C, C, ID, Max}) ->
  % grow circle CA
  {CA, Size} = maps:get(IDCA, C),
  CA2 = [QB|CA],
  Size2 = Size + 1,
  C2 = maps:update(IDCA, {CA2, Size2}, C),
  P2C2 = maps:put(QB, IDCA, P2C),
  Max2 = max(Max, Size2),
  {P2C2, C2, ID, Max2};
update(QA, QB, undefined, IDCB, {P2C, C, ID, Max}) ->
  % grow circle CB
  {CB, Size} = maps:get(IDCB, C),
  CB2 = [QA|CB],
  Size2 = Size + 1,
  C2 = maps:update(IDCB, {CB2, Size2}, C),
  P2C2 = maps:put(QA, IDCB, P2C),
  Max2 = max(Max, Size2),
  {P2C2, C2, ID, Max2};
update(QA, QB, IDCA, IDCA, {P2C, C, ID, Max}) ->
  % nothing new
  {P2C, C, ID, Max};
update(QA, QB, IDCA, IDCB, {P2C, C, ID, Max}) ->
  % combine circles CA and CB (circles are disjoint)
  {CA, SizeA} = maps:get(IDCA, C),
  {CB, SizeB} = maps:get(IDCB, C),
  CAB = combine(CA, CB, SizeA, SizeB),
  Size2 = SizeA + SizeB,
  {P2C2, IDCAB} = reindex(CA, CB, SizeA, SizeB, IDCA, IDCB, P2C),
  C2 = maps:update(IDCAB, {CAB, Size2}, C),
  Max2 = max(Max, Size2),
  {P2C2, C2, ID, Max2}.


% updating the person to circle index map seems to be costly
%
% now we reindex only one of the two circles instead of both
% and only the smaller one (this optimization is needed too)
%
% and we stuff the combined circle under the index of the larger circle,
% so that its indices can stay the same
% we keep the smaller circle in memory, to not use time to delete it
reindex(CA, CB, SizeA, SizeB, IDCA, IDCB, P2C) when SizeA > SizeB ->
  reindex(CB, IDCA, P2C);
reindex(CA, CB, SizeA, SizeB, IDCA, IDCB, P2C) ->
  reindex(CA, IDCB, P2C).

reindex(C, ID, P2C) ->
  P2C2 = lists:foldl(fun(P, P2CP) ->
    maps:update(P, ID, P2CP)
  end, P2C, C),
  {P2C2, ID}.


% CA ++ CB would be fast enough, so this optimization was not needed
combine(CA, CB, SizeA, SizeB) when SizeA > SizeB ->
  combine(CB, CA);
combine(CA, CB, _, _) ->
  combine(CA, CB).

% the resulting list order is not important here, so no reversal is needed
combine([], L2) ->
  L2;
combine([H|T], L2) ->
  combine(T, [H|L2]).


% convert the raw list into a list of pairs
pairs(L) ->
  pairs(L, []).

pairs([], L) ->
  lists:reverse(L);
pairs([H1, H2 | T], L) ->
  pairs(T, [{H1, H2}|L]).


read_list(N) ->
  Fmt = unicode:characters_to_list(lists:duplicate(N, "~d")),
  {ok, L} = io:fread("", Fmt),
  L.

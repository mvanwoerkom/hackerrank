-module(solution).
-export([main/0]).

main() ->
  {ok, [N, D]} = io:fread("", "~d ~d"),
  L = read_list(N),
  rotate_left(L, N, D),
  ok.


rotate_left(L, N, D) when D =:= N ->
  write_list(L),
  io:format("~n");
rotate_left(L, _N, D) ->
  {L1, L2} = lists:split(D, L),
  write_list(L2),
  write_list(L1),
  io:format("~n").


read_list(N)->
  Line = io:get_line(""),
  string:lexemes(Line, " " ++ [$\r, $\n]).


write_list([]) ->
  ok;
write_list([H|T]) ->
  io:format("~s ", [H]),
  write_list(T).

-module(solution).
-export([main/0]).


main() ->
  {ok, [N]} = io:fread("", "~d"),
  L = read_list(N),
  Pairs = count(L),
  io:format("~p~n", [Pairs]),
  true.


count(L) ->
  Map = maps:new(),
  count(L, Map).

count([], Map) ->
  pairs(Map);
count([H|T], Map) ->
  Map2 = maps:update_with(H, fun(V) ->
    V+1
  end, 1, Map),
  count(T, Map2).


pairs(Map) ->                  
  maps:fold(fun(_K, V, Pairs) ->
    Pairs + V div 2
  end, 0, Map).


read_list(N) ->
  Fmt = unicode:characters_to_list(lists:duplicate(N, "~d")),
  {ok, L} = io:fread("", Fmt),
  L.

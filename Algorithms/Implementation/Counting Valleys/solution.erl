-module(solution).
-export([main/0]).


main() ->
  {ok, [N]} = io:fread("", "~d"),
  L = read_list(N),
  Result = count(L),
  io:format("~p~n", [Result]),
  true.


count(L) ->
  Region = sea, 
  Level = 0,
  Valleys = 0,
  move(L, Region, Level, Valleys).

move([], _Region, _Level, Valleys) ->
  Valleys;
move([[$D]|T], mountain, 1, Valleys) ->
  move(T, sea, 0, Valleys);
move([[$D]|T], sea, 0, Valleys) ->
  move(T, valley, -1, Valleys);
move([[$D]|T], Region, Level, Valleys) ->
  move(T, Region, Level-1, Valleys);
move([[$U]|T], valley, -1, Valleys) ->
  move(T, sea, 0, Valleys+1);
move([[$U]|T], sea, 0, Valleys) ->
  move(T, mountain, 1, Valleys);
move([[$U]|T], Region, Level, Valleys) ->
  move(T, Region, Level+1, Valleys).
  
  
read_list(N) ->
  Fmt = unicode:characters_to_list(lists:duplicate(N, "~c")),
  {ok, L} = io:fread("", Fmt),
  L.

# Counting Valleys

## Problem
https://www.hackerrank.com/challenges/counting-valleys/problem

A warm up.

## Solution

I used the states `(Region, Level, Valleys)`, where `Region` is either 
`sea`, `mountain` or `valley`, `Level` is an integer and `Valleys` is the 
number of hiked valleys so far.

This led to some detailed state changes when climbing down or up across 
different regions and more general changes when staying within a particular
region.

```mermaid
graph TB;
  A["(mountain, 1, Valleys)"] -->|D| B["(sea, 0, Valleys)"]
  B -->|D| C["(valley, -1, Valleys)"]
  
  D["(Region, Level, Valleys)"] -->|D| E["(Region, Level-1, Valleys)"]

  A2["(valley, -1, Valleys)"] -->|U| B2["(sea, 0, Valleys+1)"]
  B2 -->|U| C2["(mountain, 1, Valleys+1)"]

  D2["(Region, Level, Valleys)"] -->|U| E2["(Region, Level+1, Valleys)"]
```

The above diagram shows that we might do well with less regions: 
`valley` and `not_valley` might be enough already.


## Experience
I tried to encode the region state in different functions first.

## After the task
A nice opportunity to try out the [gitlab markup](https://gitlab.com/help/user/markdown.md), which features 
[mermaid diagrams](https://mermaidjs.github.io/) and TeX/LaTeX math rendering.

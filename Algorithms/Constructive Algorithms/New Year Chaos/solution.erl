-module(solution).
-export([main/0]).

main() ->
  {ok, [T]} = io:fread("", "~d"),
  lists:foreach(fun(_I) ->
    {ok, [N]} = io:fread("", "~d"),
    L = read_list(N),
    Result = unbribe(N, L),
    write(Result)
  end, lists:seq(1, T)),
  true.


unbribe(1, _L) ->
  0;
unbribe(N, L) ->
  L0 = lists:duplicate(N, 0),
  LZ = lists:zip(L, L0),
  LZ2 = merge_sort(N, LZ),
  case ok_count(LZ2) of
    true ->
      sum_count(LZ2);
    false ->
      too_chaotic
  end.


ok_count([]) ->
  true;
ok_count([{_X, Count}|T]) ->
  case Count =< 2 of
    true ->
      ok_count(T);
    false ->
      false
  end.


sum_count(L) ->
  lists:foldl(fun({_X, Count}, Sum) ->
    Sum + Count
  end, 0, L).


merge_sort(1, L) ->
  L;
merge_sort(N, L) ->
  N2 = N div 2,
  Left = lists:sublist(L, 1, N2),
  Right = lists:sublist(L, N2+1, N),
  Left2 = merge_sort(N2, Left),
  Right2 = merge_sort(N-N2, Right),
  merge(Left2, Right2).


merge([], L2) ->
  L2;
merge(L1, []) ->
  L1;
merge(L1, L2) ->
  [{E1, Count1}=H1|T1] = L1,
  [{E2, Count2}=H2|T2] = L2,
  case E1 < E2 of
    true ->
      [H1|merge(T1, L2)];
    false ->
      [H2|merge(inc(L1), T2)]
  end.


inc(L) ->
  lists:map(fun({E, Count}) ->
    {E, Count+1}
  end, L).


write(too_chaotic) ->
  io:format("Too chaotic~n");
write(Result) ->
  io:format("~p~n", [Result]).


read_list(N) ->
  Fmt = unicode:characters_to_list(lists:duplicate(N, "~d")),
  {ok, L} = io:fread("", Fmt),
  L.

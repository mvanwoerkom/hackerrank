-module(solution).
-export([main/0]).

main() ->
  {ok, [P]} = io:fread("", "~d"),
  lists:foreach(fun(_I) ->
    SA = string:chomp(io:get_line("")),
    SB = string:chomp(io:get_line("")),
    two_strings(SA, SB)
  end, lists:seq(1, P)),
  true.


two_strings(SA, SB) ->
  MA = map(SA),
  MB = map(SB),
  case disjoint(MA, MB) of
    false ->
      Result = "YES";
    true ->
      Result = "NO"
  end,
  io:format("~s~n", [Result]).


disjoint(MA, MB) ->
  case maps:size(MA) =< maps:size(MB) of
    true ->
      disjoint2(maps:keys(MA), MB);
    false ->
      disjoint2(maps:keys(MB), MA)
  end.

disjoint2([], _Map) ->
  true;
disjoint2([H|T], Map) ->
  case maps:is_key(H, Map) of
    true ->
      false;
    false ->
      disjoint2(T, Map)
  end.


% add characters from list to hash map
map(L) ->
  map(L, maps:new()).

map([], Map) ->
  Map;
map([H|T], Map) ->
  case maps:is_key(H, Map) of
    true ->
      Map2 = Map;
    false ->
      Map2 = maps:put(H, t, Map)
  end,
  map(T, Map2).

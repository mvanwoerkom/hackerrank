-module(solution).
-export([main/0]).

main() ->
  {ok, [Year]} = io:fread("", "~d"),
  {D, M, Y} = day_of_the_programmer(Year),
  io:format("~2..0B.~2..0B.~4..0B~n", [D, M, Y]),
  ok.

% no leap: 
%   Jan+..+Aug=243 => day 256 is September, 13th
% normal leap year:
%   September, 12th
% 1918: 
%   no leap year, 13 days cut => September, 26th
day_of_the_programmer(Year) when Year =< 1917 ->
  % Julian calendar
  case (Year rem 4) =:= 0 of 
    true ->
      {12,9,Year};
    false ->
      {13,9, Year}
  end;
day_of_the_programmer(Year) when Year >= 1919 ->
  % Gregorian calendar
  case (Year rem 400) =:= 0 of 
    true ->
      {12,9,Year};
    false ->
      case ((Year rem 4) =:= 0) andalso ((Year rem 100) =/= 0) of
        true ->
          {12,9,Year};
        false ->
          {13,9,Year}
      end
  end;
day_of_the_programmer(Year) when Year =:= 1918 ->
  % transition Julian to Gregorian calender
  {26,9,Year}.

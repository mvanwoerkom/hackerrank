# Day of the Programmer

## Problem
https://www.hackerrank.com/challenges/day-of-the-programmer

## Solution
This problem is a bit unwieldy thanks to it's case distinctions.

I started with a general version, that would have determined
the date of the k-th day. As it turned longer and longer I dropped it.
So I just stuck to determining the 256-th day.

It was easiest to sum up the months in a shell and notice that
the date was in September.
Then handle the different leaps.

I had to fiddle a bit with the format specifiers, the
Erlang version is so close to C, but it wants a B instead of a d.

# Fighting Armies

## Problem
https://www.hackerrank.com/challenges/fighting-armies/problem


## Solution
DELAYED: Will be published at a later point in time.

## Experience
As the categorie "Functional Structures" suggests this is about
choosing the right data structure for the job.

I thought I had a good data structure chosen and implemented a solution.

As some tests failed due to time-outs I had to determine what part was too slow.
My candidate was the reading of the input.
But profiling showed it was good enough.

Profiling was more work than expected:

I tried first on Windows, but the task needs reading lines with varying number of arguments and this seemed not to work properly under Windows.

Next I tried the Linux from a rented test server. My impression was that it behaved similar to the hackerrank environment, but it had not enough memory on that machine. Dang!

Finally I used a virtual machine with FreeBSD which I usually use for something completely different. I believe here I got the impression that the behaviour is maybe not the fault of Windows but rather the used Erlang version.
Anyway, I was able to profile properly under FreeBSD.

And this revealed that my data structure, a (SPOILER), was not good enough for this task.

So I decided to rather implement a (SPOILER).
This needed learning the details of the proper variant of this data structure.
Thanks to good documentation, I managed to come up with a working implementation and, as hoped, it was fast enough. Nice.

## After the task
There was only one other solution. It turned out interesting.

It featured a different way to achieve fast data reading which I will try out at some point in the future.

The pride about my implementation of the (SPOILER) data structure crumbled
somewhat as the other solution demonstrated that an ordinary
(SPOILER) data structure would have been good enough already.
I was too pessimistic for that one. So more profiling next time!
# Elementary Watson

## Problem
https://www.hackerrank.com/challenges/elementary-watson/problem

One has to implement a WatLog interpreter for customer Dr. Watson.
Watlog is a logical inference system.
The name might be a reference to Prolog.

WatLog can store facts and inference rules in a knowledge base and permits to run queries on that knowledge base.

## Solution

DELAYED: Will be published at a later point in time.

## Experience

It is always cool to see something Prolog-like, because it uses logical inference for computation.
This is hinted at in the insertion sort test case.

I still hand wrote lexer and parser, because I like it, but this was a lot of code.

My solution should do the job on syntactic correct programs, but I am not sure if it would handle mal-formed input well.

Preparation started with revisiting my solution to the
[While language HackerRank problem](https://gitlab.com/mvanwoerkom/hackerrank/tree/master/Functional%20Programming/Interpreter%20and%20Compilers/While%20Language)
and a look into Prof. Wirth's compiler book to fresh up parser writing.

I then started to read through the literature, to fresh up my knowledge on logical inference and the unification algorithm, as I remembered from a lecture on functional
and logic programming, that it was an important part of Prolog implementations.

For this I used a well known standard text.
It gave pseudo code for some important algorithms.
But I was not happy with this pseudo code. It might have been too vague or even buggy or I did not understand it well.
I started to look at the code samples which exist for that text but had no quick insight.

The problem is lying dormant right now, I hope to pick it up again at some later point in time.

## After the task
TBD
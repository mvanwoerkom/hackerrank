# Puzzle and PC

## Problem
https://www.hackerrank.com/challenges/puzzle-and-pc/problem

Cover a square NxN board, minus one given cell, with L-shaped trominoes.

## Solution
DELAYED: Will be published at a later point in time.

## Experience
This was a very satisfying problem.

At first I thought this would need a very complicated solution.
Especially that given hole looked troublesome.
But it turns out that the hole is rather a friend than an enemy.

The solution is mathematical pleasing and simple once one understood it.


## After the task
Surprise, surprise. Only one other solution. No plagiarism. Yea.

# Super-Queens on a Chessboard

## Problem
https://www.hackerrank.com/challenges/super-queens-on-a-chessboard/problem

A variation of the classic puzzle with queens on a chessboard.
With more powerful queens.

## Solution
DELAYED: Will be published at a later point in time.

## Experience
Can not remember anything special.

## After the task
More original solutions than usual. Only two entries were plagiarized, as usual from tamarit27. And this for 20 points only.

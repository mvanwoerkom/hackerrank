# The Tree Of Life

## Problem
https://www.hackerrank.com/challenges/the-tree-of-life/problem

A cellular automaton inspired by Conway's Game of Life, but not
on a rectangular grid but on a binary tree.

## Solution

DELAYED: Will be published at a later point in time.

## Experience
Task description was fine, except how to calculate the value of boundary nodes,
which have less than three neighbours.

I started with the calculation of the new value of a node, depending on the given rule
and old values of parent, node, and children.

The bit fiddling I first did C style, using arithmetic bitshifting (bsl) but after the first few lines,
I felt the urge to refresh my bit syntax.
The docs are somewhat dry here. I had to try some attempts in an Erlang shell until I got the statements correct.
I like the result. It would be interesting to compare performance with the C style version.

The value function got used to implement a function which outputs a given rule in the format of the task description.
So I was able to check my implementation by comparing the outputs.

Then I started the main function. It reads the values from standard input and writes the results.

The value function, which for a given tree extracts the node value selected by a path string, was easier
than expected.

Then I coded the parser which constructs a tree from the tree string.
I managed to come up with a recursive descent parser, a look at the sources of the while language parser
helped to set it up. I still enjoy coding these manually.
This parser was simpler. I left out the error propagation and organized the parser not fully like the
grammar,

The representation of the tree changed slightly, I thought about using a map to hold the trees to have
faster node access, but in the end tuples were good enough. Though I switched from using atoms x and o
for X and . as values to plain 1 and 0 integers. This I did to avoid transformations between atoms and
integers.

The evaluation of a tree, yielding the new tree took two attempts. I forgot to transform
the whole tree first and then I noticed that (X1, X2, X3, X4) = (Parent, Left, Node, Right) instead
of (Parent, Node, Left, Right) after I could not reproduce the trees from the example.

Then I wrote the code to perform the tree calculation for the given points in time.

The first version, starting from 0 for times that were reached with a negative step,
otherwise moving forward, worked for 9 of the 10 test cases. For test case 3 it was too slow.

Here an optimization, memorizing old results and minimizing new calculations,
made it fast enough to pass test case 3 too. It has still room for more optimization.

## After the task

I had a look at the editorial. It featured solutions in Haskell.
Some stuff felt familiar, much felt not.
They seem to have a higher level view than me, regarding functional
programming.
Need to revisit this some day.

Looking through the other solutions revealed the usual cheaters.





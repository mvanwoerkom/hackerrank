# Expressions V2

## Problem
https://www.hackerrank.com/challenges/expressions-v2/problem

A calculator with modular arithmetic and
right associative operators for a change.
```julia
p - q - r = p - (q - r)
```

## Solution
DELAYED: Will be published later.

I recycled
[Simplify the Algebraic Expressions](https://gitlab.com/mvanwoerkom/hackerrank/tree/master/Functional%20Programming/Parsers/Simplify%20the%20Algebraic%20Expressions)
because it had the unary operator, alas I had to remove the recognition of "a a" terms (where the multiplication operator has been omitted).

### Pass 1: Lexical Analysis

Example: The expression
```julia
4/-2/(2 + 8)
```

is turned into
```erlang
Tokens=[{num,4,0},
        {op_a,op_div,1},
        {op_a,op_sub,2},
        {num,2,3},
        {op_a,op_div,4},
        {paren_open,5},
        {num,2,6},
        {op_a,op_add,8},
        {num,8,10},
        {paren_close,11}]
```

where we have only the column number as extra argument, used for nicer error messages.

The list of tokens:
```erlang
%   X
%   {op_minus, Tree}
%   {op_add, TreeA1, TreeA2}
%   {op_sub, TreeA1, TreeA2}
%   {op_mult, TreeA1, TreeA2}
%   {op_div, TreeA1, TreeA2}
```

### Pass 2: Syntactic Analysis

The parser is written to suit this grammar:
```erlang
%   a -> a_term (+ a_term | - a_term)^*
%   a_term -> a_factor (* a_factor | / a_factor)^*
%   a_factor -> +^{0,1} a_factor | -^{0,1} a_factor | n | ( a )
```

The example expression 
```julia
4/-2/(2 + 8)
```

results in
```erlang
Tree={op_div,4,{op_div,{op_minus,2},{op_add,2,8}}}
```

I tried to achieve right associative operators by means of the
grammar first:
```erlang
%   left associative:
%     a -> a_term + a | a_term
%     a_term -> a_factor * a_term | a_factor
%   right associative:
%     a -> a + a_term | a_term
%     a_term -> a_term * a_factor | a_factor
```
but it seemed easier to change the way how the syntax tree was
accumulated.


### Pass 3: Evaluation
The usual modular arithmetic. We avoid negative results.
And division is performed by multiplying with the modular inverse.


## After the task


## Keywords
compiler, interpreter, parser, recursive descent parser

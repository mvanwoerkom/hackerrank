# Bitter Chocolate

## Problem
https://www.hackerrank.com/challenges/bitter-chocolate/problem

A two player game about biting from a chocolate each, but
avoiding the bitter piece.

## Solution
DELAYED: Will be published at a later point in time.

## Experience
This was easy, once a couple of vacation days put me into relaxed state.


## After the task

6 of the now 10 solutions were not original.

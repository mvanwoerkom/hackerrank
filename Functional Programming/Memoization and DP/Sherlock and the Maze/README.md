# Sherlock and the Maze

## Problem
https://www.hackerrank.com/challenges/sherlock-and-the-maze/problem

How many paths exist from location (1, 1) to (N, M) with at most K turns?

## Solution
DELAYED: Will be published at a later point in time.

## Experience
Looked tougher than it turned out to be.

I solved it on paper mainly. It helped to try to solve the sample
test cases on paper.

After some trials the recursion got simpler and the proper data structures got clearer.
Then it became clear in what order to perform the calculation. 
I ended up with a solution which could have been memory optimized if necessary.

## After the task
You guessed it, I had a look at the other solutions.

Only the solutions by vychodil_hynek and tamarit27 were original work.
The other four entries were, as usual, copied from tamarit27. Sigh.

# hackerrank
This repository contains some of my submissions to [hackerrank](http://www.hackerrank.com).

I enjoy banging my head against the challenges using [Erlang](http://www.erlang.org).
If this seems not possible I tell them to my friend [Ruby](http://www.ruby-lang.org).
